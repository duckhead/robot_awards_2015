// The 3pi include file must be at the beginning of any program that
// uses the Pololu AVR library and 3pi.
#include <pololu/3pi.h>

// This include file allows data to be stored in program space.  The
// ATmega168 has 16k of program space compared to 1k of RAM, so large
// pieces of static data should be stored in program space.
#include <avr/pgmspace.h>

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <math.h>


struct __attribute__ ((__packed__)) serial_package
{
	char type;
	uint16_t x;
	uint16_t y;	
};

typedef struct serial_package serial_package;
serial_package cmd_list[8];


float euclidean_distance(uint16_t x_1, uint16_t y_1, 
		uint16_t x_2, uint16_t y_2)
{
	return sqrt(pow(((double)(x_2-x_1)),2) + pow(((double)(y_2-y_1)),2));
}


void syncronize(void)
{
	uint8_t sync_data;
	do
	{
		serial_receive_blocking((char*)&sync_data, 1, 100);
		if(0xFF == sync_data)
		{
			serial_receive_blocking((char*)&sync_data, 1, 100);
				if(0xFF == sync_data)
					{
						break;
					}
		}
	}while(1);
}



void get_serial_package(){

	int i;
	uint8_t num_packages;

	syncronize();
	serial_receive_blocking((char*)&num_packages, 1, 10000);
	clear();

	for(i=0; i<num_packages; i++)
	{
		serial_receive_blocking((char*)&cmd_list[i], 5, 1000);	
		if(0 == i)
			printf("%4d%4d",cmd_list[i].x, cmd_list[i].y);	
	}
}


void initialize()
{
	// This must be called at the beginning of 3pi code, to set up the
	// sensors.  We use a value of 2000 for the timeout, which
	// corresponds to 2000*0.4 us = 0.8 ms on our 20 MHz processor.

	pololu_3pi_init(2000);
}


void print_LCD(const char *text)
{
	clear();
	print(text);
}



void set_speed(const int speed1, const int speed2)
{
	set_m1_speed(speed1);
	set_m2_speed(speed2);
}


void turn_left()
{
	set_speed(-100, 100);
	delay(100);
	set_speed(0, 0);	
}


void turn_right()
{
	set_speed(100, -100);
	delay(100);
	set_speed(0, 0);	
}



// This is the main function, where the code starts.  All C programs
// must have a main() function defined somewhere.
int main()
{

	uint16_t p_x, p_y, g_x, g_y, diff_x, diff_y, diff_new_x, diff_new_y ;

	// set up the 3pi
	initialize();
	serial_set_baud_rate(115200);

	clear();

	lcd_init_printf();

	while(1){
		get_serial_package();
		
		p_x = cmd_list[0].x;
		p_y = cmd_list[0].y;
		g_x = cmd_list[1].x;
		g_y = cmd_list[1].y;
		diff_x = abs(p_x-g_x);
		diff_y = abs(p_y-g_y);

		//drive for a short time to get heading
		set_speed(50,50);
		delay(250);
		set_speed(0,0);
		
		get_serial_package();

		diff_new_x = abs(cmd_list[0].x-g_x);
		diff_new_y = abs(cmd_list[0].y-g_y);		
		if(diff_new_x > diff_x )
			turn_left();
		else if(diff_new_y > diff_y )
			turn_right();
	}
}